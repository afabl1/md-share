import Link from "next/link";

import { Metadata } from "next";

export const metadata: Metadata = {
  title: "MD Share",
  description: "Share your markdown files, faster than anything else",
};

export default function Home() {
  return (
    <main>
      <h1>MD Share</h1>

      <p>Share your markdown files, faster than anything else</p>

      <ul>
        <li>
          <Link href="/notes-on-supertokens">Notes on supertokens</Link>
        </li>
      </ul>
    </main>
  );
}
