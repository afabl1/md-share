import { ReactNode } from "react";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Notes on Supertokens | MD Share",
};

export default function NotesOnSupertokens({
  children,
}: {
  children: ReactNode;
}) {
  return children;
}
